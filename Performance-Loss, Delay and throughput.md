---
tags:
  - kap-1
  - lp3
  - Performance
aliases:
  - Kap-1.3
---
[[Kapitel 1 - Intro]]
# Contents
- [[#How does packet loss and delay occur?]]
- [[#Packet Delay]]
	- [[#“Real” internet delays and routes]]
- [[#Packet loss]]
- [[#Throughput]]
--- 

# How does packet loss and delay occur?

- Packets queue in router buffers, waiting for turn for transmission
	- Queue length grows when arrival rate to link (temp) exceeds occurs output link capacity
- Packet loss occurs when memory to hold queued packets fills up

# Packet Delay

### $d_{proc}$: nodal processing
- check bit errors
- Determine output link
- Typically < microsecs

### $d_{queue}:$ queueing delay
- time waiting at output link for transmission
- depends on the congestion level of the router

### $d_{Trans}:$ Transmission delay
- $L:$ packet length (bits)
- $R:$ link transmission delay (bps)
- $d_{trans} =\frac{L}{R}$

### $d_{prop}:$ Propagation delay
- $d:$ length of physical link
- $s:$ propagation speed ($2 \cdot 10^{8}$m/s)

# Packet loss

- queue preceding link in buffer has finite capacity
- Packet arriving to full queue dropped
- Lost packet may be retransmitted by previous node, by source end system, or not at all
# Throughput

- Throughput: rate (bits/time units) at which bits are being sent from sender to reciver
	- Instantaneous: rate at given point in time
	- Average: rate over longer period of time