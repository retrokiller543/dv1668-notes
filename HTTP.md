---
tags:
  - lager-5
  - http
  - Network-apps
  - Chapter2
---
[[Chapter 2 - Layer 5]]
# Contents
```table-of-contents
style: nestedList # TOC style (nestedList|inlineFirstLevel)
minLevel: 0 # Include headings from the specified level
maxLevel: 0 # Include headings up to the specified level
includeLinks: true # Make headings clickable
debugInConsole: false # Print debug info in Obsidian console
```
---

# HTTP
#http
## Overview
- Web’s application-layer protocol
- Client/server model:
	- *Client:* browser that requests, receives, (using HTTP protocol) and “displays” Web objects
	- *Server:* Web server sends (using HTTP protocol) objects in response to requests

## Flow
A web server can take sever requests at the same time without disturbing any other connections as long as it has the resources for it

```mermaid
sequenceDiagram

PC->>+Server: HTTP request

IPhone->>+Server: HTTP request
Server->>-IPhone: HTTP response
Server->>-PC: HTTP response
```
#flow-chart #http #example
## HTTP uses TCP

### Flow for a single connection
**dotted lines symbol the TCP connection and the filled in line is HTTP specific**

```mermaid
sequenceDiagram

PC-->>+Server: Initiate TCP connection

Server-->>-PC: Accept the connection

PC->>+Server: HTTP message

Server->>-PC: HTTP message

PC-->>+Server: Close TCP Connection

Server-->>-PC: TCP connection closed
```
#flow-chart #http 
## HTTP is “stateless”
- server maintains *no* information about past client requests
## Types of HTTP connections
### Non-persistent
Downloading multiple objects require multiple connections

1. TCP connection opened
2. At most one object sent over TCP connection
3. TCP connection closed

#### Example
```mermaid
sequenceDiagram
    participant C as Client
    participant S as Server

    Note over C,S: Non-Persistent HTTP Connection
    C->>S: Establish TCP Connection
    S-->>C: Acknowledge TCP Connection
    C->>S: Send HTTP Request
    S-->>C: Process Request
    S->>C: Send HTTP Response
    C-->>S: Acknowledge Receipt
    C->>S: Close TCP Connection
    S-->>C: Acknowledge Closure
    Note over C,S: TCP Connection Closed

```
#flow-chart #http-message
#### Issues
- requires 2 RTTs per object
- OS overhead for *each* TCP connection
- Browsers ofter open multiple parallel TCP connections to fetch referenced objects in parallel

### Persistent
- TCP connection opened to server
- Multiple objects can be sent over *single* TCP connection between client, and that server
- TCP connection closed

### Additional info
- server leaves connection open after sending response
- Subsequent HTTP messages between same client/server sent over open connection
- Client sends requests as soon as it encounters a referenced object
- As little as one RTT for all the referenced objects (halving the response time)

## Messages

### Message Format
```mermaid
graph TD

A[Method]

B[SP]

C[URL]

D[SP]

E[Version]

F[CR + LF]

G[Headers Start]

H1n[header field name]

H1v[value]

H1cl[CR + LF]

H2n[header field name]

H2v[value]

H2cl[CR + LF]

H[Headers End]

I[CR + LF]

J[Body]

  

A --> B

B --> C

C --> D

D --> E

E --> F

F --> G

G --> H1n --> H1v --> H1cl

G --> H2n --> H2v --> H2cl

H1cl --> H

H2cl --> H

H --> I

I --> J
```
#format #http-message 
### Requests messages

#### POST method
- web page often includes form input
- User input sent from client to server in entity body of HTTP POST request message

#### GET method
**For sending data to server**
- include user data in URL field of HTTP GET request message (following a `?`)
	www.example.com/path?data=value

#### HEAD method
- requests headers (only) that would be returned *if* specified URL were requested with an HTTP GET method

#### PUT method
- uploads new file (object) to server
- Completely replaces file that exists at specified URL with content in entity body of POST HTTP request message

### Response codes
#response-codes
#### Ranges
##### 100-199
Informational responses
##### 200-299
Successful responses
##### 300-399
Redirection messages
##### 400-499
Client-error responses
##### 500-599
Server-error responses
#### 200 OK
Request succeeded, request object later in this message
#### 301 Moved Permanently
Requested object moved, new location specified later in this message (in `Location: field`)
#### 400 Bad Request
Request message not understood by server
#### 404 Not Found
Requested document not found on this server
#### 505 HTTP Version Not Supported
## User/server State
#cookies
### Overview
HTTP itself is stateless, but state can be achieved using cookies

- No notion of multi-step exchanges of HTTP messages to complete a web “transaction”
	- No need for client/server to track “state” of multi-step exchange
	- All HTTP requests are independent of each other
	- No need for client/server to ”recover” from a partially-completed-but-never-completely-completed transaction
### Basics
Websites and client browsers use *cookies* to maintain some state between transactions
### Four components
1) Cookie header line of HTTP *response* message
2) Cookie header line in next HTTP *request* message
3) Cookie file kept on user’s host, managed by user’s browser
4) Back-end database at Website
### Usages
- Authorization
- Shopping carts
- Recommendations
- User session state (Web e-mail)
## Caches
#cache
A cache’s purpose is to satisfy client requests without involving origin server

- User configures browser to point to a (local) *Web cache*
- Browser sends all HTTP requests to cache
	- *If* object in cache: cache returns object to client
	- *Else* cache requests object from origin server, caches received object, then returns object to client
### Overview
- Web cache acts as both client and server
	- Server for original requesting client
	- Client to origin server
- Server tells cache about object’s allowable caching in response header:
	`Cache-Control: max-age=<seconds>`
	`Cache-Control: no-cache`
#### Why?
- reduce response time for client requests
	- Cache is closer to client
- Reduce traffic on an institution’s access link
- Internet is dense with caches
	- Enables ‘poor’ content providers to more effectively deliver content