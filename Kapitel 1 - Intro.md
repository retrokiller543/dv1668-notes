---
tags:
  - kap-1
  - intro
  - lp3
aliases:
  - Kap-1
---
[[Intro]]

--- 
### Introduktion

- Vad betyder allt?
- Prestandan kring nätverk, etc

### A ”nuts and bolts” view 
#kap-1/basic-view-of-internet 
- Billions of connected devices
	- Hosts = end systems (devices)
	- Running network apps at internet’s ”edge”
- Packet Switches
	- Forward packets (chunks of data)
	- Router, switches
- Communication links
	- Fiber, copper, radio, satellite
	- Transmission rate: bandwidth
- Networks
	- Collection of devices, routers, links: managed by an organization

## Contents

- [[The basics|Kap-1.1]]
- [[Performance-Loss, Delay and throughput]]