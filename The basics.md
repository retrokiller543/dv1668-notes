---
tags:
  - kap-1
  - lp3
aliases:
  - Kap-1.1
---
[[Kapitel 1 - Intro]]
## Contents
- [[#Types of Connections]]
	- [[#Home networks]]
	- [[#Wireless networks]]
		- [[#WLAN]]
		- [[#Cellular]]
	- [[#Enterprise networks]]
- [[#Host]]
- [[#Links]]
	- [[#Twisted pair (TP)]]
	- [[#COAX]]
	- [[#Fiber optic]]
- [[#Networking Core]]
	- [[#Forwarding]]
	- [[#Routing]]
	- [[#Packet-switching]]
		- [[#store and forward]]
		- [[#Queing and loss]]
--- 
## Types of Connections 
#connection-types
- COAX - Copper connection
- Twisted pair (RJ45) 
- Fiber
- Mobile (radio)
- ADSL
### Home networks
- router to make the connection outwards for all connections in the network
- WiFi from access points
- Wired connections (RJ45)
- Firewall usually in the router
- NAT (network address translation)

### Wireless networks
#wireless
Shared from a basestation aka “access point”

#### WLAN
#wireless #wlan
- Typically within or around building (roughly 30m)
- WiFi 4, 5, 6, etc

#### Cellular
#wireless #cellular
- provided by mobile, cellular network operator (10km)
- 10 Mbps
- 4G cellular network and 5G cellular network

### Enterprise networks
#enterprise-networking
- Used by companies, universities, etc
- Mix of wireless and wired connections

## Host
#the-host
The host sends *packets* of data

Host sending function: 
- takes application message
- Breaks into smaller chunks, known as *packets*, of length *L* bits
- Transmits packets into access network at transmission rate R
	- Link transmission rate, aka link capacity, aka link bandwith $$\text{packet transmission delay} = \frac{L(bits)}{R\left( \frac{bits}{s} \right)}$$
## Links
#links
- bit: propagates between transmitter/reciver pairs
- Physical link: what lies between transmitter & reciver
- Guided media: 
	- Signals propagate in solid media: copper, fiber, coax
- Unguided media: 
	- Signals propaget freely, e.g., radio
### Twisted pair (TP)
#rj45
- Two insulated copper wires
	- CAT 5: 100Mbps, 1Gbps Ethernet
	- CAT 6: 10Gbps Ethernet
### COAX
#coax
- Two concentric copper conductors
- Bidirectional
- Broadband:
	- Multiple frequency channels on cable
	- 100’s Mbps per channel
### Fiber optic
#fibre
- Glass fiber carrying light pulses, each pulse a bit
- High-speed
	- 10-100Gbps
- Low error rate

## Networking Core

### Forwarding
What the destination is

- switching
- local action:
	- Move arriving packets from router’s input link to appropriate router output link

### Routing
What path to take

- Global action:
	- determine source destination paths taken by packets
- Routing algorithms

### Packet-switching
#### store and forward
- packet transmission delay:
	- Takes $\frac{L}{R}$ seconds to transmit (push out) L-bit packet into link at R bps

#### Queing and loss
- packets can be stored in a buffer if load is high
- Can cause loss of packets aka *packet loss*

