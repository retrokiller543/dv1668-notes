---
tags:
  - lager-5
  - Network-apps
  - Chapter2
aliases:
  - L5
  - Net-apps
---
[[Chapter 2 - Layer 5]]
# Contents
```table-of-contents
```

---

# Overview

- Web and HTTP
- SMTP, IMAP, POP3
- DNS
- P2P apps
- Video streaming and CDN
- Socket programming with UDP & TCP

# Network Apps

## Examples

- Web
- Text messaging
- Email
- VOIP

## Creating

### Writing programs that

- run on (different) end systems
- Communicate over network
- e.g. web server communicates with browser

### Notes

There is no need to write software for network-core devices

- These should not run user apps
- Apps on end systems allows for rapid development, propagation

## Client-server paradigm

### Server

- Always on host
- Permanant IP address

### Clients

- Contact, communicate with server
- May be intermittently connected
- May have dynamic IP adresses
- Do not communicate directly with each other
- Examples: HTTP, IMAP, FTP

## Peer-peer architecture

- no always-on server
- Arbitary end systems directly communicate
- Peers request service from other peers, provide service in return to other peers
    - Self _scalability_ - new peers bring new service capacity, as well as new service demands
- Peers are intermittently connected and change IP addresses
    - Complex management
- Example: P2P file sharing

## Processes communication

### Process

A program running within a host

- within same host, two processes communicate using **inter-process communication** (defined by OS)
- Processes in different host communicate by exchanging **messages**

#### Clients & servers

**Client process:** process that initiates communication
**Server process:** process that waits to be contacted

### Notes

Applications with P2P architectures have client & server processes

### Sockets

- Process sends/receives messages to/from its socket
- Socket analogous to door
    - Sending process shoves message out the door
    - Sending process relies on transport infrastructure on other side of the door to deliver the message to the socket at the receiving process
    - Two sockets involved: one on each side

### Addressing processes

- To receive messages, process must have _identifier_
- Host device has unique 32-bit IP address
- Many processes can be running on a host and therefor is the IP address not enough for identifying
- _identifier_ includes both a _IP Address_ and _port numbers_ associated process on host

## App-layer protocol

These define things such as

- types of messages exchanged
- Message syntax
- Message semantics
- Rules

### Open protocols

- defined in RFCs, everyone has access to protocol definition
- Allows for interoperability

### Closed protocols

- e.g. skype, zoom

## Transport protocols

### Transport service requirements in common apps

| Application            | Data Loss     | Throughput                             | Time Sensitive?  |
| ---------------------- | ------------- | -------------------------------------- | ---------------- |
| File transfer/download | No loss       | Elastic                                | No               |
| E-mail                 | No loss       | Elastic                                | No               |
| Web documents          | No loss       | Elastic                                | No               |
| Real-time audio/video  | Loss-tolerant | Audio: 5Kbps-1Mbps Video: 10Kbps-5Mbps | Yes, 10ms        |
| Streaming audio/video  | Loss tolerant | Same as above                          | Yes, few seconds |
| Interactive games      | Loss tolerant | Kbps+                                  | Yes, 10ms        |
| Text messaging         | No loss       | Elastic                                | Yes and no       |

### Services

#### TCP

- _Reliable_
- _Flow control_
- _Congestion-oriented_
- _Connection-oriented_

##### Downsides

Does not provide any timing, minimum throughput guarantee, security

#### UDP

- _Unreliable data transfer_

##### Downsides

Does not provide:

- reliability
- Flow control
- Congestion control
- Timing
- Throughput guarantee
- Security
- Connection setup

### Apps and what transport protocols they use

| Application            | Application layer protocol                     | Transport protocol |
| ---------------------- | ---------------------------------------------- | ------------------ |
| File transfer/download | FTP [RFC 959]                                  | TCP                |
| E-mail                 | SMTP [RFC 5321]                                | TCP                |
| Web documents          | HTTP 1.1 [RFC 7320]                            | TCP                |
| Internet telephony     | SIP [RFC 3261], RTP [RFC 3550], or proprietary | TCP or UDP         |
| Streaming audio/video  | HTTP [RFC 7320], DASH                          | TCP                |
| Interactive games      | WOW, FPS (proprietary)                         | TCP or UDP         |
