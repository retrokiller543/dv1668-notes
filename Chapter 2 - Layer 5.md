---
tags:
  - Chapter2
  - lager-5
---
[[Intro]]
# Chapter contents
- [[Principles of network apps]]
- [[HTTP]]

# Info
Application layer is the highest level abstraction in the network when looking at network layers.

# Chapter Canvas
![[Chapter 2.canvas|Chapter 2]]