---
tags:
  - intro
---
# Tables of contents
- [[Kapitel 1 - Intro]]
	- [[The basics|Kap-1.1]]
	- [[Chapter 2 - Layer 5]]
--- 

#### Remember this shit!
#must-remeber

- must not forget things
- E-D is bad, must get C+ in course (AK will be mad otherwise)

## Vad betyder datakommunikation & internet 

Ca 50 års sen första mejlet skickades.
Många olika protokoll som t.ex http, https.

## Att lära sig/kunna mot slutet av kursen
#vad-vi-kommer-att-lära-oss

- Förstå hur grundläggande principer
- Lära sig de vanligaste protokollen och deras syfte, TCP, HTTP, SMTP, DNS, etc
- Lära sig ett nytt språk, teknikspråket med alla förkortningar
- Protokoll-stacken, boken går igenom den från topp till botten
- Utföra enkel socket-programmering (nätverksprogrammering) (python??)
- Observera/tolka nätverkstrafik till och från en dator
- Förstå och konfigurera lokala nätverk, switchar och routrar
- Förstå hur nätverk kopplas samman och hur ett meddelande (packet) tar sig från sändande dator till mottagande dator (routing)
- Genomföra ett nätverksprojekt där du bygger ett större nätverk (10-12 pers) i slutet av maj
- Kunna använda kunskapen och kunnandet inom t.ex. nätsäk

Uppgifter kommer från boken

Lp 4 använder vi mat från Cisco academy

## Protokoll-stacken (Lp 3)
#lp3

Vi kommer gå igenom ett lager varje vecka, förutom sista lagret, fysiska lagret, inte med i denna kurs, första veckan är endast övergripande om nät begrepp och info

Applikationslagret - lager 5 #lager-5
Transportlagret - Lager 4 #lager-4
Nätverkslagret, data och styrning - lager 3 #lager-3
Länklagret och LAN - lager 2 #lager-2
Fysiska lagret - lager 1 #lager-1

## Lp 4
#lp4

 - Switchade nätverk
	 - VLAN, STP, Etherchannel
- Konfigurera av router, subnetting, inter-VLAN routing
- DHCP
- WLAN (WiFi)
- Statisk routing
- Dynamic routing m.h.a OSPF
- Enkel nätverkssäkerhet, Access Control Lists (ACL)
- NAT (Network Adress Translation)
- IPv4
- IPv6

## Info om dokumentation
- Moduler i canvas
- Föreläsningar och problemlösning på förelsäningar
- Diskussioner i canvas
- Discord länk finns på canvas

## Examationer

12p totalt, 6 på tentan, 6 på praktiska delar. Första tentan sker i Mars. 3st under Lp 3 och 2st under Lp 4 plus ett projekt i slutet av Lp 4

Labb sker indiviudellt ifall man inte har 2st datorer annars i grupp

G411 och G410 #salar

Vi kan ladda ner Cisco Packet Tracer (dock inte vid redovisning) super bra för att experimentera på egen hand utan fysisk hårdvara eller att vara i labb salarna.

Nytt OS kommer vi behöva lära oss, kommer från Cisco. Likt Linux?

## Socketprogrammering
Startar i slutet av vecka 2. Bygg en webbserver/”skapa en egen webb-browser” kan göras tillsamans.

